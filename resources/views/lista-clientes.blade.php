<style>
    h1 {
        text-align: center;
        font-family: sans-serif;
    }
    table {
        text-align: center;
        font-family: sans-serif;
        border-collapse: collapse
    }
    td {
        padding: 5px;
    }
    #linha {
        background: #d3d3d3;
        font-weight: bold;
    }
</style>
<h1>Lista de clientes cadastrados</h1>
<table border="">
    <tr id="linha" >
        <td>id</td>
        <td>Nomes</td>
        <td>Email</td>

        <td>CPF</td>
        <td>Telefone</td>
        <td>data de nasc</td>
    </tr>

    @foreach ( $lista_clientes as $lista_cliente )
        <tr>
            <td>{{ $lista_cliente->id }}</td>
            <td>{{ $lista_cliente->nome }}</td>
            <td>{{ $lista_cliente->email }}</td>
            <td>{{ $lista_cliente->cpf }}</td>
            <td>{{ $lista_cliente->telefone }}</td>
            <td>{{ date('d-m-Y', strtotime($lista_cliente->datadenasc)) }}</td>
        
        </tr>
    @endforeach
</table>