## Resumo do que já foi feito

**Atividade:** Listar clientes do banco de dados. Os dados são: id, nome, email, cpf, telefone e data de nascimento.

- Rota /clientes foi criado para listar os clientes. Uma view especifica para listar esses clientes.

- Migration cridado com os campos necessários.

- Seed criada para inserir 9 dados cada vez que executar.


### Migration
```
Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 50);
            $table->string('email', 30);
            $table->char('cpf', 11);
            $table->char('telefone', 11);
            $table->date('datadenasc');
            $table->timestamps();
        });
```

### Seed
```
for ($i = 1; $i <= 9; $i++){
            DB::table('clientes')->insert([
                'nome'       => 'Joao '.Str::random(4),
                'email'      => 'joao'.Str::random(4).'@gmail.com',
                'cpf'        => '1234567891'.$i,
                'telefone'   => '6334567891'.$i,
                'datadenasc' => '1999-01-12',
        ]);
        }
```
### Tecnologias utilizadas
- XAMPP com php 7.4 e o banco de dados
- Laravel 6