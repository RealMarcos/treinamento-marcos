<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        for ($i = 1; $i <= 9; $i++){
            DB::table('clientes')->insert([
                'nome'       => 'Joao '.Str::random(4),
                'email'      => 'joao'.Str::random(4).'@gmail.com',
                'cpf'        => '1234567891'.$i,
                'telefone'   => '6334567891'.$i,
                'datadenasc' => '1999-01-12',
        ]);
        }
    }
}